{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "8c0d572c-3978-4c07-bd3d-35cfe7692f83",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "source": [
    "# Why C++?\n",
    "\n",
    "C++ is a very large programming language. \n",
    "It has several layers of sophisticated concepts. It is\n",
    "also very deep. It takes a long time to learn \n",
    "(at least months) and takes years of use to master.\n",
    "\n",
    "So before you embark on your journey of learning C++, you should first ask yourself\n",
    "why? Python is a significantly easier to learn language which requires many fewer\n",
    "lines of code to be typed to achieve the same result as C++.\n",
    "\n",
    "However, despite its complexity, C++ is my personal favourite language. C++ is large and complex\n",
    "because programming computers is a large and complex task to undertake. What Python\n",
    "hides for the sake of simplicity, C++ makes obvious. As such, C++ does not hold\n",
    "your hand and make things easy. Instead, it provides you with a complete suite\n",
    "of tools and concepts that give you complete control over how your program\n",
    "uses the underlying hardware of the computer.\n",
    "\n",
    "C++ is a multi-paradigm multi-level programming language. You can use it to write\n",
    "programs in pretty much any paradigm (e.g. structural, object orientated, functional),\n",
    "and at any level (e.g. assembly/register, processor, domain specific, parallel data centre).\n",
    "\n",
    "This power and flexibility comes at the cost of forcing you to be aware of all of \n",
    "these concepts, and to write more lines of code to fully specify everything\n",
    "you want to achieve.\n",
    "\n",
    "In addition, the power and flexibility of C++ comes with no safety barriers or\n",
    "helpful hints. The language gives you great power, which comes with great\n",
    "responsibility. You can write programs that will crash terribly, that will have\n",
    "massive security holes and that will be more convoluted than spaghettified coral and\n",
    "more unreadable than a parliamentary answer that has been translated into Latin by \n",
    "Google.\n",
    "\n",
    "It is thus your responsibility to use C++ in the \"right way\". The language provides\n",
    "many useful tools and many well-structured ways of programming that you can use\n",
    "to write safe, secure, readable and sustainable software. However, it is your \n",
    "responsibility to choose and use them."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "15163ae8-6bb9-49e3-a39c-5a6e492df989",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "source": [
    "## Benefits of C++\n",
    "\n",
    "Assuming you use the tools provided, and follow a good coding style, then C++ \n",
    "enables you to write powerful, sustainable software that is many times\n",
    "faster, and significantly easier to support than anything written using \n",
    "a scripting language such as Python or R. It will take you longer to write\n",
    "C++ compared to Python or R, but the result will be an executable that \n",
    "is portable, uses the full power of the processor (or processors), and\n",
    "that is structured in a way that makes the software easier to support and\n",
    "continue to develop as part of a team.\n",
    "\n",
    "C++ is a compiled language. This means that executables produced from\n",
    "C++ code are, by default, many many times faster than interpreters \n",
    "processing scripting languages (e.g. such as Python). An example of this\n",
    "is the comparison of the Python program `metropolis.py` and the equivalent\n",
    "C++ program `metropolis.cpp`, which was written for the\n",
    "[Monte Carlo workshop](https://chryswoods.com/intro_to_mc/).\n",
    "The Python script takes 8.5 seconds\n",
    "on my laptop to complete 5000 Monte Carlo moves. The C++ program takes\n",
    "2.4 seconds to complete 500,000 moves. Both programs are written \n",
    "naively as teaching examples with no attempt at optimisation. Yet, the\n",
    "C++ program is around 350 times faster. If I were to optimise both programs,\n",
    "then I am confident that the C++ code would optimise better, and could\n",
    "be up to 1000 times faster than Python.\n",
    "\n",
    "Because C++ is a compiled language, and compilers exist for pretty much\n",
    "every hardware platform, it means that standard C++ code is highly \n",
    "portable. Assuming you stick to the standard, you can write software\n",
    "that can compile and run on the full range of operating systems (Windows,\n",
    "Unix, OS X, Linux, WebOS, iOS, Android), the full range of processors\n",
    "(x86, Arm, Power, Sparc, MIPS), and on\n",
    "the full range of hardware (desktops, supercomputers, Mars rovers,\n",
    "IoT devices, smartphones, tablets, control computers for microscopes or\n",
    "particle colliders etc. etc.) available."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "41330f36-321a-4e62-b8cd-505e63ff9198",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "source": [
    "## History of C++\n",
    "\n",
    "Another reason to learn C++ is because it has a strong heritage, and is \n",
    "one of the few universally important programming languages for which there\n",
    "is a strong demand for programmers now, and will likely stay in demand\n",
    "for decades to come.\n",
    "\n",
    "C++ was first conceived in 1979 by Bjane Stroustrup, and was motivated as a way\n",
    "to add support for object orientated programming to C. It started off being\n",
    "known as \"C with Classes\". As such, C++ includes (nearly) all of C, and valid C code is valid C++ code.\n",
    "\n",
    "The language became known as C++ around 1983. The name is a play on the increment\n",
    "operator (`++`), and implies that C++ is an improved/incremented version of C \n",
    "(there is a language called D which took this further, but is not widely used).\n",
    "\n",
    "C++ evolved through the 80's and 90's, with increased availability of compilers,\n",
    "and a \"standard\" which consisted of published reference books. This changed in\n",
    "1998, when the C++ standards committee finally published an official standard\n",
    "for the language (C++98). The standard library that comes with C++, which \n",
    "is known as the STL (standard template library) was included in C++98.\n",
    "However, the committee had to respond to several issues with the standard,\n",
    "resulting in the revision, which was later dubbed C++03.\n",
    "\n",
    "In 2005 work began on developing new features for C++, particularly focussed\n",
    "on making the language easier to use, and adding in support for functional\n",
    "programming (a lot of this work was influenced by the\n",
    "[Boost library project](http://www.boost.org)). The proposed standard was called C++0x, on the assumption that\n",
    "work would be completed before 2010. As it was, the standard was not released\n",
    "until 2011, meaning that it was called C++11.\n",
    "\n",
    "Further to this, small refinements were made to the standard to again improve ease\n",
    "of use and fix small issues with C++11. The revisions were released in 2014,\n",
    "meaning that this standard is called C++14. Today, all modern C++ compilers \n",
    "fully support C++14, and there is no reason not to take advantage of the \n",
    "tools it provides. In particular, modern C++14 allows you to write in a \n",
    "coding style that is very similar to Python :-)\n",
    "\n",
    "C++ continues to evolve, with the C++17 standard released in 2017 and C++20 now under\n",
    "discussion. This evolution ensures that C++ keeps up with the cutting edge of\n",
    "programming paradigms and new software applications. However, as refinements\n",
    "to the standard are backwards compatible, and are additions to the language,\n",
    "they do not affect old code. C++ code written in the 80's and 90's is still valid\n",
    "C++ and will happily compile using a modern C++14 compiler. This multi-decade\n",
    "stability gives confidence that you can write software today that will still\n",
    "be valuable, usable and supportable in decades to come."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a143158f-b79a-49b3-8c80-2528ef065c6c",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "source": [
    "## Summary (TL;DR)\n",
    "\n",
    "So, in summary, use C++ if you are focussed on how long your code takes to run, rather than how long your code takes to write. Use C++ if your code is going to grow and be developed by a large number of people. The extra cost today will be balanced by easier maintainability in the future. Finally, use C++ if you want portable code that can be compiled and will work today, and for decades to come."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
